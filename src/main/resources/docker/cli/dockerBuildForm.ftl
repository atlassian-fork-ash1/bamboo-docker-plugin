[@s.textfield labelKey='docker.repository' name='repository' cssClass="long-field" required=true /]

[@ww.radio labelKey='docker.dockerfile' name='dockerfileOption' toggle='true'
    list='dockerfileOptions' listKey='first' listValue='second' /]
[@ui.bambooSection dependsOn='dockerfileOption' showOn='inline']
    [@ww.textarea name='dockerfile' cssClass="long-field" required=true rows="8" /]
[/@ui.bambooSection]

[@s.checkbox labelKey='docker.nocache' name='nocache' /]
[@s.checkbox labelKey='docker.save' toggle='true' name='save' /]
[@ui.bambooSection dependsOn='save' showOn='true']
    [@s.textfield labelKey='docker.save.filename' name='filename' cssClass="long-field" required=true /]
[/@ui.bambooSection]

[@s.textfield labelKey='docker.build.options' name='buildOptions' cssClass="long-field" /]