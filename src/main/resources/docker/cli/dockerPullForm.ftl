[#include "../macro.ftl" /]
[@ww.radio labelKey='docker.registry' name='pullRegistryOption' toggle='true'
list='registryOptions' listKey='first' listValue='second' /]
[@s.textfield labelKey='docker.pull.repository' name='pullRepository' cssClass="long-field" required=true /]
[@ui.bambooSection dependsOn='pullRegistryOption' showOn='hub']
    <div class="description">[@s.text name='docker.pull.repository.hub.description' /]</div>
[/@ui.bambooSection]
[@ui.bambooSection dependsOn='pullRegistryOption' showOn='custom']
    <div class="description">[@s.text name='docker.pull.repository.custom.description' /]</div>
[/@ui.bambooSection]
<br/>
[@ui.bambooSection titleKey="docker.credentials"]
    <div class="description">[@s.text name='docker.credentials.description'/]</div>
    [@s.radio name='pullCredentialsSource'
        listKey='key' listValue='value' toggle=true required=true
        list=credentialsSources cssClass='radio-group' /]
    [@ui.bambooSection dependsOn="pullCredentialsSource" showOn="USER"]
        [@s.textfield labelKey='docker.username' name='pullUsername' cssClass="long-field" required=true /]
        [#if pullPassword?has_content]
            [@s.checkbox labelKey='docker.password.change' toggle=true name='pullChangePassword' /]
            [@ui.bambooSection dependsOn='pullChangePassword']
                [@s.password labelKey='docker.password' name='pullPassword' cssClass="long-field" required=true/]
            [/@ui.bambooSection]
        [#else]
            [@s.hidden name='pullChangePassword' value='true' /]
            [@s.password labelKey='docker.password' name='pullPassword' cssClass="long-field" required=true/]
        [/#if]
        [@s.textfield labelKey='docker.email' name='pullEmail' cssClass="long-field" /]
    [/@ui.bambooSection]
    [@ui.bambooSection dependsOn="pullCredentialsSource" showOn="SHARED_CREDENTIALS"]
        [#if stack.findValue("noSharedCredentials")!false]
            [@noCredentialsMessageBox id='sharedCredentials.infoBox'/]
        [#else]
            [@s.select
            id='pullSharedCredentialsId'
            labelKey='docker.task.sharedCredentials'
            name='pullSharedCredentialsId'
            list=sharedCredentials
            listKey='first'
            listValue='second']
                [@s.param name="headerKey"]-1[/@s.param]
                [@s.param name="headerValue"][@s.text name='docker.sharedCredentials.default'/][/@s.param]
            [/@s.select]
        [/#if]
    [/@ui.bambooSection]
[/@ui.bambooSection]