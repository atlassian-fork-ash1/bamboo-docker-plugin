package com.atlassian.bamboo.plugins.docker.process;

import com.atlassian.utils.process.ExternalProcess;
import com.atlassian.utils.process.ExternalProcessBuilder;
import com.atlassian.utils.process.ProcessException;
import com.atlassian.utils.process.StringInputHandler;
import com.atlassian.utils.process.StringOutputHandler;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.Path;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;

public class DefaultDockerProcessService implements DockerProcessService
{
    private final ImmutableMap<String, String> environmentVariables;
    private final File workingDir;

    private DefaultDockerProcessService(@NotNull final Builder builder)
    {
        this.environmentVariables = ImmutableMap.copyOf(builder.environmentVariables);
        this.workingDir = builder.workingDir;
    }

    @NotNull
    public String execute(@NotNull final ProcessCommand command) throws ProcessException
    {
        final StringInputHandler input = Optional.ofNullable(command.getInput())
                .filter(StringUtils::isNotEmpty)
                .map(StringInputHandler::new)
                .orElse(null);
        final StringOutputHandler output = new StringOutputHandler();
        final StringOutputHandler error = new StringOutputHandler();
        workingDir.mkdirs();

        ExternalProcessBuilder processBuilder = new ExternalProcessBuilder()
                .command(command.getCommandList(), workingDir)
                .env(environmentVariables);
        processBuilder = input != null
                ? processBuilder.handlers(input, output, error)
                : processBuilder.handlers(output, error);
        final ExternalProcess externalProcess = processBuilder.build();

        final int exitCode;

        try
        {
            externalProcess.execute();
            exitCode = externalProcess.getHandler().getExitCode();
        }
        catch (Throwable e)
        {
            throw new ProcessException("Could not execute '" + command.getSafeCommandString() + "': " + error.getOutput(), e);
        }
        finally
        {
            externalProcess.cancel();
        }

        if (exitCode != 0)
        {
            throw new ProcessException("Error executing '" + command.getSafeCommandString() + "', exit code: " + exitCode, exitCode);
        }

        return output.getOutput().trim();
    }

    @NotNull
    @Override
    public String executeSilently(@NotNull ProcessCommand command) throws ProcessException
    {
        return execute(command);
    }

    @Override
    public Path getWorkingDirectory() {
        return workingDir != null ? workingDir.toPath() : null;
    }

    @Override
    public Map<String, String> getEnvironmentVariables() {
        return environmentVariables;
    }

    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private Map<String, String> environmentVariables = Collections.emptyMap();
        private File workingDir;

        private Builder()
        {
        }

        public Builder environmentVariables(@NotNull final Map<String, String> environmentVariables)
        {
            this.environmentVariables = environmentVariables;
            return this;
        }

        public Builder workingDir(@NotNull final File workingDir)
        {
            this.workingDir = workingDir;
            return this;
        }

        @NotNull
        public DefaultDockerProcessService build()
        {
            return new DefaultDockerProcessService(this);
        }
    }
}
