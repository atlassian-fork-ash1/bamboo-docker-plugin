package com.atlassian.bamboo.plugins.docker.upgrade;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.crypto.instance.SecretEncryptionService;
import com.atlassian.bamboo.deployments.environments.Environment;
import com.atlassian.bamboo.deployments.environments.service.EnvironmentTaskService;
import com.atlassian.bamboo.deployments.projects.DeploymentProject;
import com.atlassian.bamboo.deployments.projects.service.DeploymentProjectService;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.TopLevelPlan;
import com.atlassian.bamboo.plugins.docker.PluginConstants;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.Message;
import com.atlassian.sal.api.upgrade.PluginUpgradeTask;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;

@BambooComponent
@ExportAsService(PluginUpgradeTask.class)
public class EncryptSecureFieldsUpgradeTask implements PluginUpgradeTask
{
    private static final Logger log = Logger.getLogger(EncryptSecureFieldsUpgradeTask.class);

    private final BuildDefinitionManager buildDefinitionManager;
    private final DeploymentProjectService deploymentProjectService;
    private final EnvironmentTaskService environmentTaskService;
    private final PlanManager planManager;

    private final SecretEncryptionService secretEncryptionService;

    @Inject
    public EncryptSecureFieldsUpgradeTask(final BuildDefinitionManager buildDefinitionManager,
                                          final DeploymentProjectService deploymentProjectService,
                                          final EnvironmentTaskService environmentTaskService,
                                          final PlanManager planManager,
                                          final SecretEncryptionService secretEncryptionService)
    {
        this.buildDefinitionManager = buildDefinitionManager;
        this.deploymentProjectService = deploymentProjectService;
        this.environmentTaskService = environmentTaskService;
        this.planManager = planManager;
        this.secretEncryptionService = secretEncryptionService;
    }

    @Override
    public int getBuildNumber()
    {
        return 2;
    }

    @Override
    public String getShortDescription()
    {
        return "Encrypt passwords for Docker tasks";
    }

    @Override
    public Collection<Message> doUpgrade()
    {
        log.info("Encrypt password fields for Docker tasks");
        encryptTaskInPlans();
        reencryptTaskInDeployments();
        return Collections.emptyList();
    }

    private void reencryptTaskInDeployments()
    {
        final AtomicInteger count = new AtomicInteger(0);
        for (DeploymentProject deploymentProject : deploymentProjectService.getAllDeploymentProjects())
        {
            for (Environment environment : deploymentProject.getEnvironments())
            {
                environment.getTaskDefinitions()
                        .stream().filter(isPlugin())
                        .forEach(taskDefinition ->
                        {
                            final Map<String, String> configuration = new HashMap<>(taskDefinition.getConfiguration());
                            boolean updated = encryptSecureFields(configuration);
                            if(updated)
                            {
                                environmentTaskService.editTask(environment.getId(), taskDefinition.getId(), taskDefinition.getUserDescription(), taskDefinition.isEnabled(), configuration);
                                count.getAndIncrement();
                            }
                        });
            }
        }
        log.info("Encrypted password fields for Docker task in deployment environments. Updated " + count.get() + " tasks.");
    }

    private void encryptTaskInPlans()
    {
        final AtomicInteger count = new AtomicInteger(0);
        for (TopLevelPlan plan : planManager.getAllPlansUnrestricted())
        {
            for (Job job : plan.getAllJobs())
            {
                int beforeUpdate = count.get();
                final BuildDefinition buildDefinition = job.getBuildDefinition();
                buildDefinition.getTaskDefinitions().stream()
                        .filter(isPlugin())
                        .forEach(taskDefinition ->
                        {
                            final Map<String, String> taskConfig = taskDefinition.getConfiguration();
                            boolean updated = encryptSecureFields(taskConfig);
                            if(updated)
                            {
                                taskDefinition.setConfiguration(taskConfig);
                                count.getAndIncrement();
                            }
                        });
                if(beforeUpdate < count.get()) //save only if updated
                {
                    buildDefinitionManager.savePlanAndDefinition(job, buildDefinition);
                }
            }
        }
        log.info("Encrypted password fields for Docker task in build plans. Updated " + count.get() + " tasks.");
    }

    @NotNull
    private Predicate<TaskDefinition> isPlugin()
    {
        return taskDefinition -> taskDefinition.getPluginKey().equals(DockerCliTaskConfigurator.DOCKER_CLI_TASK_KEY);
    }

    private boolean encryptSecureFields(Map<String, String> config)
    {
        boolean updated = encrypt(DockerCliTaskConfigurator.PASSWORD, config);
        updated = encrypt(DockerCliTaskConfigurator.PULL_PASSWORD, config) || updated;
        return updated;
    }

    /**
     * @return true if config was updated
     */
    private boolean encrypt(String key, Map<String, String> config)
    {
        boolean updated = false;
        if (config.containsKey(key))
        {
            final String oldValue = config.get(key);

            if (StringUtils.isNotEmpty(oldValue) && !secretEncryptionService.isEncrypted(oldValue))
            {
                String encryptedValue = secretEncryptionService.encrypt(oldValue);
                config.put(key, encryptedValue);
                updated = true;
            }
        }
        return updated;
    }

    @Override
    public String getPluginKey()
    {
        return PluginConstants.DOCKER_PLUGIN_KEY;
    }
}
