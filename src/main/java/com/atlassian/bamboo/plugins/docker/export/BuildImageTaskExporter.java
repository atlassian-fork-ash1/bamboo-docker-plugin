package com.atlassian.bamboo.plugins.docker.export;

import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.model.task.docker.AbstractDockerTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerBuildImageTaskProperties;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.BUILD_OPTIONS;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE_OPTION_EXISTING;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKERFILE_OPTION_INLINE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.FILENAME;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.NOCACHE;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REPOSITORY;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.SAVE;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUBDIRECTORY;

@BambooComponent
public class BuildImageTaskExporter implements TaskDefinitionExporter {

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull TaskProperties taskProperties) {
        DockerBuildImageTaskProperties properties = Narrow.downTo(taskProperties, DockerBuildImageTaskProperties.class);
        Preconditions.checkArgument(properties != null, "Don't know how to import " + taskProperties.getClass().getCanonicalName());

        ImmutableMap.Builder<String, String> builder = ImmutableMap.builder();

        builder.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_BUILD);
        builder.put(REPOSITORY, properties.getImageName());

        switch (properties.getDockerfileContent()) {
            case INLINE:
                builder.put(DOCKERFILE_OPTION, DOCKERFILE_OPTION_INLINE);
                builder.put(DOCKERFILE, StringUtils.defaultString(properties.getDockerfile()));
                break;
            case WORKING_DIR:
                builder.put(DOCKERFILE_OPTION, DOCKERFILE_OPTION_EXISTING);
                break;
            default:
                throw new IllegalStateException("Couldn't handle: " + properties.getDockerfileContent());

        }

        builder.put(NOCACHE, String.valueOf(!properties.isUseCache()));
        builder.put(SAVE, String.valueOf(properties.isSaveAsFile()));
        builder.put(FILENAME, StringUtils.defaultString(properties.getImageFilename()));
        builder.put(BUILD_OPTIONS, StringUtils.defaultString(properties.getAdditionalArguments()));
        builder.put(CFG_ENVIRONMENT_VARIABLES, StringUtils.defaultString(properties.getEnvironmentVariables()));
        builder.put(CFG_WORKING_SUBDIRECTORY, StringUtils.defaultString(properties.getWorkingSubdirectory()));
        return builder.build();
    }

    @NotNull
    @Override
    public Task toSpecsEntity(@NotNull TaskDefinition taskDefinition) {
        if (isDockerBuildTask(taskDefinition)) {
            Map<String, String> config = taskDefinition.getConfiguration();

            final DockerBuildImageTask dockerTask = new DockerBuildImageTask()
                    .imageName(config.getOrDefault(REPOSITORY, ""))
                    .workingSubdirectory(config.getOrDefault(CFG_WORKING_SUBDIRECTORY, ""))
                    .environmentVariables(config.getOrDefault(CFG_ENVIRONMENT_VARIABLES, ""));

            switch (config.getOrDefault(DOCKERFILE_OPTION, "")) {
                case DOCKERFILE_OPTION_INLINE:
                    dockerTask.dockerfile(config.getOrDefault(DOCKERFILE, ""));
                    break;
                case DOCKERFILE_OPTION_EXISTING:
                    dockerTask.dockerfileInWorkingDir();
                    break;
                default:
                    throw new IllegalStateException("Couldn't handle: " + config.getOrDefault(DOCKERFILE_OPTION, ""));
            }

            boolean noCache = Boolean.parseBoolean(config.getOrDefault(NOCACHE, "false"));
            dockerTask.useCache(!noCache);

            boolean saveAsFile = Boolean.parseBoolean(config.getOrDefault(SAVE, "false"));
            dockerTask.saveAsFile(saveAsFile)
                    .imageFilename(config.getOrDefault(FILENAME, ""))
                    .additionalArguments(config.getOrDefault(BUILD_OPTIONS, ""));

            return dockerTask;
        }

        throw new IllegalArgumentException(String.format("Couldn't export task by id: %s and plugin key: %s", taskDefinition.getId(), taskDefinition.getPluginKey()));
    }

    private boolean isDockerBuildTask(@NotNull TaskDefinition taskDefinition) {
        return taskDefinition.getPluginKey().equals(AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey())
                && DOCKER_COMMAND_OPTION_BUILD.equals(taskDefinition.getConfiguration().get(DOCKER_COMMAND_OPTION));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext, @NotNull TaskProperties taskProperties) {
        return Collections.emptyList();
    }
}
