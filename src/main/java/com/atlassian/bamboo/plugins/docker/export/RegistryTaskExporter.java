package com.atlassian.bamboo.plugins.docker.export;

import com.atlassian.bamboo.credentials.CredentialsAccessor;
import com.atlassian.bamboo.credentials.CredentialsData;
import com.atlassian.bamboo.credentials.UsernamePasswordCredentialType;
import com.atlassian.bamboo.crypto.instance.SecretEncryptionService;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.specs.api.builders.credentials.SharedCredentialsIdentifier;
import com.atlassian.bamboo.specs.api.builders.task.Task;
import com.atlassian.bamboo.specs.api.model.credentials.SharedCredentialsIdentifierProperties;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.api.validators.common.ValidationProblem;
import com.atlassian.bamboo.specs.builders.task.AbstractDockerRegistryTask;
import com.atlassian.bamboo.specs.builders.task.DockerPullImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.model.task.docker.AbstractDockerTaskProperties;
import com.atlassian.bamboo.specs.model.task.docker.DockerRegistryTaskProperties;
import com.atlassian.bamboo.task.TaskContainer;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import com.atlassian.bamboo.task.export.TaskValidationContext;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.plugin.spring.scanner.annotation.component.BambooComponent;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.EMAIL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PASSWORD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_EMAIL;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_PASSWORD;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_REGISTRY_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_REPOSITORY;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_SHARED_CREDENTIALS_ID;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PULL_USERNAME;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PUSH_REPOSITORY;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.PUSH_SHARED_CREDENTIALS_ID;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REGISTRY_OPTION;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REGISTRY_OPTION_CUSTOM;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.REGISTRY_OPTION_HUB;
import static com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator.USERNAME;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_ENVIRONMENT_VARIABLES;
import static com.atlassian.bamboo.task.TaskConfigConstants.CFG_WORKING_SUBDIRECTORY;
import static java.util.Collections.singletonList;

@BambooComponent
public class RegistryTaskExporter implements TaskDefinitionExporter
{

    private static final Set<String> ACCEPTABLE_COMMAND_OPTIONS = ImmutableSet.of(DOCKER_COMMAND_OPTION_PULL, DOCKER_COMMAND_OPTION_PUSH);

    private final CredentialsAccessor credentialsAccessor;
    private final SecretEncryptionService secretEncryptionService;

    @Inject
    public RegistryTaskExporter(@BambooImport CredentialsAccessor credentialsAccessor,
                                @BambooImport SecretEncryptionService secretEncryptionService)
    {
        this.credentialsAccessor = credentialsAccessor;
        this.secretEncryptionService = secretEncryptionService;
    }

    @NotNull
    @Override
    public Map<String, String> toTaskConfiguration(@NotNull TaskContainer taskContainer, @NotNull TaskProperties taskProperties)
    {
        final DockerRegistryTaskProperties properties = Narrow.downTo(taskProperties, DockerRegistryTaskProperties.class);
        Preconditions.checkArgument(properties != null,
                "Don't now how to import " + taskProperties.getClass().getCanonicalName());

        ImmutableMap.Builder<String, String> config = ImmutableMap.builder();

        switch (properties.getOperationType())
        {
            case PUSH:
                config.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PUSH);
                config.put(DockerCliTaskConfigurator.EMAIL, StringUtils.defaultString(properties.getEmail()));
                config.put(DockerCliTaskConfigurator.USERNAME, StringUtils.defaultString(properties.getUsername()));
                config.put(DockerCliTaskConfigurator.PASSWORD, encryptIfRequired(properties.getPassword()));
                config.put(DockerCliTaskConfigurator.PUSH_SHARED_CREDENTIALS_ID, getSharedCredentialsId(properties));
                config.put(DockerCliTaskConfigurator.PUSH_REPOSITORY, StringUtils.defaultString(properties.getImage()));
                config.put(REGISTRY_OPTION, getRegistryType(properties.getRegistryType(), taskProperties));
                break;
            case PULL:
                config.put(DOCKER_COMMAND_OPTION, DOCKER_COMMAND_OPTION_PULL);
                config.put(DockerCliTaskConfigurator.PULL_EMAIL, StringUtils.defaultString(properties.getEmail()));
                config.put(DockerCliTaskConfigurator.PULL_USERNAME, StringUtils.defaultString(properties.getUsername()));
                config.put(DockerCliTaskConfigurator.PULL_PASSWORD, encryptIfRequired(properties.getPassword()));
                config.put(DockerCliTaskConfigurator.PULL_SHARED_CREDENTIALS_ID, getSharedCredentialsId(properties));
                config.put(DockerCliTaskConfigurator.PULL_REPOSITORY, StringUtils.defaultString(properties.getImage()));
                config.put(PULL_REGISTRY_OPTION, getRegistryType(properties.getRegistryType(), taskProperties));
                break;
            default:
                throw new IllegalStateException("Couldn't handle operation type: " + properties.getOperationType());
        }

        config.put(CFG_ENVIRONMENT_VARIABLES, StringUtils.defaultString(properties.getEnvironmentVariables()));
        config.put(CFG_WORKING_SUBDIRECTORY, StringUtils.defaultString(properties.getWorkingSubdirectory()));

        return config.build();
    }

    @NotNull
    private String encryptIfRequired(@Nullable String password)
    {
        if (StringUtils.isEmpty(password))
        {
            return "";
        }
        else if (secretEncryptionService.isEncrypted(password))
        {
            return password;
        }
        else
        {
            return secretEncryptionService.encrypt(password);
        }
    }

    @NotNull
    @Override
    public Task toSpecsEntity(@NotNull TaskDefinition taskDefinition)
    {
        if (isDockerRegistryTask(taskDefinition))
        {
            Map<String, String> config = taskDefinition.getConfiguration();
            switch (config.getOrDefault(DOCKER_COMMAND_OPTION, ""))
            {
                case DOCKER_COMMAND_OPTION_PUSH:
                    DockerPushImageTask pushTask = new DockerPushImageTask()
                            .environmentVariables(config.getOrDefault(CFG_ENVIRONMENT_VARIABLES, ""))
                            .workingSubdirectory(config.getOrDefault(CFG_WORKING_SUBDIRECTORY, ""));
                    setAuthorization(taskDefinition, config, pushTask, PUSH_SHARED_CREDENTIALS_ID, USERNAME, PASSWORD, EMAIL);
                    return setDockerImage(pushTask, config.getOrDefault(REGISTRY_OPTION, ""), config.getOrDefault(PUSH_REPOSITORY, ""));
                case DOCKER_COMMAND_OPTION_PULL:
                    DockerPullImageTask pullTask = new DockerPullImageTask()
                            .environmentVariables(config.getOrDefault(CFG_ENVIRONMENT_VARIABLES, ""))
                            .workingSubdirectory(config.getOrDefault(CFG_WORKING_SUBDIRECTORY, ""));
                    setAuthorization(taskDefinition, config, pullTask, PULL_SHARED_CREDENTIALS_ID, PULL_USERNAME, PULL_PASSWORD, PULL_EMAIL);
                    return setDockerImage(pullTask, config.getOrDefault(PULL_REGISTRY_OPTION, ""), config.getOrDefault(PULL_REPOSITORY, ""));
                default:
                    throw new IllegalStateException("Something went badly. Couldn't export.");
            }
        }

        throw new IllegalArgumentException(String.format("Couldn't export task by id: %s and plugin key: %s", taskDefinition.getId(), taskDefinition.getPluginKey()));
    }

    @NotNull
    @Override
    public List<ValidationProblem> validate(@NotNull TaskValidationContext taskValidationContext,
                                            @NotNull TaskProperties taskProperties)
    {
        final DockerRegistryTaskProperties properties = Narrow.downTo(taskProperties, DockerRegistryTaskProperties.class);
        if (properties == null)
        {
            return singletonList(new ValidationProblem("Don't now how to import " + taskProperties.getClass().getCanonicalName()));
        }
        final ValidationContext dockerTaskValidationContext = ValidationContext.of("Docker task");
        if (properties.getSharedCredentialsIdentifier() != null)
        {
            final String sharedCredentialsName = properties.getSharedCredentialsIdentifier().getName();
            final CredentialsData credentials = credentialsAccessor.getCredentialsByName(sharedCredentialsName);
            final ValidationContext sharedCredentialsContext = dockerTaskValidationContext.with("Shared credentials");
            if (credentials == null)
            {
                return singletonList(new ValidationProblem(sharedCredentialsContext, "Can't find shared credentials with name '" + sharedCredentialsName + "'"));
            }
            if (!UsernamePasswordCredentialType.PLUGIN_KEY.equals(credentials.getPluginKey()))
            {
                return singletonList(new ValidationProblem(sharedCredentialsContext, "Shared credentials '" + credentials.getName() + "' should be Username/password type"));
            }
        }
        else
        {
            if (!StringUtils.isEmpty(properties.getPassword()) && secretEncryptionService.isEncrypted(properties.getPassword()))
            {
                try
                {
                    secretEncryptionService.decrypt(properties.getPassword());
                }
                catch (Exception e)
                {
                    return singletonList(new ValidationProblem(dockerTaskValidationContext, "Can't decrypt password for registry task. Check if it's valid for given Bamboo instance"));
                }
            }
        }
        return Collections.emptyList();
    }

    private String getSharedCredentialsId(DockerRegistryTaskProperties properties)
    {
        final SharedCredentialsIdentifierProperties sharedCredentials = properties.getSharedCredentialsIdentifier();
        if (sharedCredentials == null)
        {
            return "";
        }

        final CredentialsData credentials = credentialsAccessor.getCredentialsByName(sharedCredentials.getName());
        if (credentials == null)
        {
            return "";
        }

        return String.valueOf(credentials.getId());
    }

    private String getRegistryType(DockerRegistryTaskProperties.RegistryType registryType, @NotNull TaskProperties taskProperties) {
        switch (registryType) {
            case DOCKER_HUB:
                return REGISTRY_OPTION_HUB;
            case CUSTOM:
                return REGISTRY_OPTION_CUSTOM;
            default:
                throw new IllegalStateException("Couldn't import: " + taskProperties);
        }
    }

    private void setAuthorization(@NotNull TaskDefinition taskDefinition, Map<String, String> config,
                                  AbstractDockerRegistryTask registryTask, String sharedCredentialsIdKey,
                                  String usernameKey, String passwordKey, String emailKey)
    {
        if (DockerCliTaskConfigurator.isSharedCredentialsSourceActive(sharedCredentialsIdKey, taskDefinition.getConfiguration()))
        {
            final long sharedCredentialsId = Long.parseLong(taskDefinition.getConfiguration().getOrDefault(sharedCredentialsIdKey, "-1"));
            final CredentialsData credentials = credentialsAccessor.getCredentials(sharedCredentialsId);
            if (credentials != null)
            {
                registryTask.authentication(new SharedCredentialsIdentifier(credentials.getName()));
            }
        }
        else
        {
            registryTask.authentication(config.getOrDefault(usernameKey, ""),
                    config.getOrDefault(passwordKey, ""),
                    config.getOrDefault(emailKey, ""));
        }
    }

    @NotNull
    private Task setDockerImage(AbstractDockerRegistryTask task, String registryOption, String registry)
    {
        switch (registryOption)
        {
            case REGISTRY_OPTION_HUB:
                return task.dockerHubImage(registry);
            case REGISTRY_OPTION_CUSTOM:
                return task.customRegistryImage(registry);
            default:
                throw new IllegalStateException("Something went badly. Couldn't export.");
        }
    }

    private boolean isDockerRegistryTask(@NotNull TaskDefinition taskDefinition)
    {
        return taskDefinition.getPluginKey().equals(AbstractDockerTaskProperties.MODULE_KEY.getCompleteModuleKey())
                && ACCEPTABLE_COMMAND_OPTIONS.contains(taskDefinition.getConfiguration().get(DOCKER_COMMAND_OPTION));
    }
}
