package com.atlassian.bamboo.plugins.docker.client;

import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;

import javax.annotation.concurrent.Immutable;

@Immutable
public class BuildConfig
{
    private final boolean nocache;
    private final String additionalArguments;

    private BuildConfig(@NotNull final Builder builder)
    {
        this.nocache = builder.nocache;
        this.additionalArguments = builder.additionalArguments;
    }

    public boolean isNoCache()
    {
        return nocache;
    }

    @NotNull
    public String getAdditionalArguments()
    {
        return additionalArguments;
    }

    @NotNull
    public static Builder builder()
    {
        return new Builder();
    }

    public static class Builder
    {
        private boolean nocache = false;
        private String additionalArguments = "";

        private Builder()
        {
        }

        @NotNull
        public Builder nocache(final boolean nocache)
        {
            this.nocache = nocache;
            return this;
        }

        @NotNull
        public Builder additionalArguments(final String additionalArguments)
        {
            this.additionalArguments = StringUtils.defaultString(additionalArguments);
            return this;
        }

        @NotNull
        public BuildConfig build()
        {
            return new BuildConfig(this);
        }
    }
}