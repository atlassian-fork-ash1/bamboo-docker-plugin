package com.atlassian.bamboo.plugins.docker.config;

import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.docker.DataVolume;
import com.atlassian.bamboo.docker.PortMapping;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.utils.predicates.text.TextPredicates;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.primitives.Ints;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collections;
import java.util.List;

public class RunConfiguration
{
    private final String image;
    private final String command;
    private final boolean runDetached;
    private final String name;
    private final PortMapping firstPort;
    private final List<PortMapping> ports;
    private final boolean link;
    private final List<DataVolume> volumes;
    private final String workDir;
    private final boolean waitForService;
    private final String serviceUrl;
    private final long serviceTimeout;
    private final String environmentVariables;
    private final String additionalArgs;

    @NotNull
    public static RunConfiguration fromContext(@NotNull final CommonTaskContext taskContext)
    {
        return new RunConfiguration(taskContext);
    }

    private RunConfiguration(@NotNull final CommonTaskContext taskContext)
    {
        final ConfigurationMap configurationMap = taskContext.getConfigurationMap();

        image = configurationMap.get(DockerCliTaskConfigurator.IMAGE);
        command = configurationMap.get(DockerCliTaskConfigurator.COMMAND);
        runDetached = configurationMap.getAsBoolean(DockerCliTaskConfigurator.DETACH);
        name = (runDetached) ? configurationMap.get(DockerCliTaskConfigurator.NAME) : null;
        firstPort = (runDetached) ? getFirstPort(configurationMap) : null;
        ports = (runDetached) ? getPorts(configurationMap) : Collections.<PortMapping>emptyList();
        link = configurationMap.getAsBoolean(DockerCliTaskConfigurator.LINK);
        volumes = getVolumes(configurationMap);
        workDir = configurationMap.get(DockerCliTaskConfigurator.WORK_DIR);

        waitForService = runDetached && configurationMap.getAsBoolean(DockerCliTaskConfigurator.SERVICE_WAIT);
        serviceUrl = configurationMap.get(DockerCliTaskConfigurator.SERVICE_URL_PATTERN);
        serviceTimeout = getServiceTimeout(configurationMap);

        environmentVariables = configurationMap.get(DockerCliTaskConfigurator.ENV_VARS);
        additionalArgs = configurationMap.get(DockerCliTaskConfigurator.ADDITIONAL_ARGS);
    }

    @NotNull
    private List<DataVolume> getVolumes(@NotNull final ConfigurationMap configurationMap)
    {
        final ImmutableList.Builder<DataVolume> volumes = ImmutableList.builder();

        for (String key : Iterables.filter(configurationMap.keySet(), TextPredicates.startsWith(DockerCliTaskConfigurator.CONTAINER_DATA_VOLUME_PREFIX)::test))
        {
            final String containerDataVolume = configurationMap.get(key);
            final String indexString = key.substring(DockerCliTaskConfigurator.CONTAINER_DATA_VOLUME_PREFIX.length());
            final String hostDirectory = StringUtils.trimToNull(configurationMap.get(DockerCliTaskConfigurator.HOST_DIRECTORY_PREFIX + indexString));

            volumes.add(new DataVolume(hostDirectory, containerDataVolume));
        }

        return volumes.build();
    }

    @Nullable
    private PortMapping getFirstPort(@NotNull final ConfigurationMap configurationMap)
    {
        final String containerPortString = configurationMap.get(DockerCliTaskConfigurator.CONTAINER_PORT_PREFIX + 0);

        if (containerPortString != null)
        {
            final String hostPortString = configurationMap.get(DockerCliTaskConfigurator.HOST_PORT_PREFIX + 0);
            final Integer hostPort = (hostPortString != null) ? Ints.tryParse(hostPortString) : null;

            return new PortMapping(Ints.tryParse(containerPortString), hostPort);
        }

        return null;
    }

    @NotNull
    private List<PortMapping> getPorts(@NotNull final ConfigurationMap configurationMap)
    {
        final ImmutableList.Builder<PortMapping> ports = ImmutableList.builder();

        for (String key : Iterables.filter(configurationMap.keySet(), TextPredicates.startsWith(DockerCliTaskConfigurator.CONTAINER_PORT_PREFIX)::test))
        {
            final Integer containerPort = Integer.parseInt(configurationMap.get(key));
            final String indexString = key.substring(DockerCliTaskConfigurator.CONTAINER_PORT_PREFIX.length());
            final String hostPortString = configurationMap.get(DockerCliTaskConfigurator.HOST_PORT_PREFIX + indexString);
            final Integer hostPort = (hostPortString != null) ? Ints.tryParse(hostPortString) : null;

            ports.add(new PortMapping(containerPort, hostPort));
        }

        return ports.build();
    }

    private long getServiceTimeout(@NotNull final ConfigurationMap configurationMap)
    {
        final String serviceTimeout = configurationMap.get(DockerCliTaskConfigurator.SERVICE_TIMEOUT);

        if (StringUtils.isBlank(serviceTimeout))
        {
            return DockerCliTaskConfigurator.DEFAULT_TIMEOUT_SECONDS;
        }

        return configurationMap.getAsLong(DockerCliTaskConfigurator.SERVICE_TIMEOUT);
    }

    public String getImage()
    {
        return image;
    }

    public String getCommand()
    {
        return command;
    }

    public boolean isRunDetached()
    {
        return runDetached;
    }

    public String getName()
    {
        return name;
    }

    public PortMapping getFirstPort()
    {
        return firstPort;
    }

    public List<PortMapping> getPorts()
    {
        return ports;
    }

    public boolean isLink()
    {
        return link;
    }

    public List<DataVolume> getVolumes()
    {
        return volumes;
    }

    public String getWorkDir()
    {
        return workDir;
    }

    public boolean isWaitForService()
    {
        return waitForService;
    }

    public String getServiceUrl()
    {
        return serviceUrl;
    }

    public long getServiceTimeout()
    {
        return serviceTimeout;
    }

    public String getEnvironmentVariables()
    {
        return environmentVariables;
    }

    public String getAdditionalArgs()
    {
        return additionalArgs;
    }
}