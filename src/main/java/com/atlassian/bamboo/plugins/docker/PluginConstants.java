package com.atlassian.bamboo.plugins.docker;

public interface PluginConstants
{
    String DOCKER_PLUGIN_KEY = "com.atlassian.bamboo.plugins.bamboo-docker-plugin";
}
