package it.com.atlassian.bamboo.plugins.docker.pageobjects;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import org.openqa.selenium.By;

public class AdvancedOptionsElement
{
    private final PageElement element;

    private final PageElement advancedOptionsHeader;
    private final PageElement advancedOptions;

    public AdvancedOptionsElement(PageElement element)
    {
        this.element = element;
        this.advancedOptionsHeader = element.find(By.cssSelector("div.summary h3"));
        this.advancedOptions = element.find(By.cssSelector("div.collapsible-details"));
    }

    public void expand()
    {
        if (!advancedOptions.isVisible())
        {
            advancedOptionsHeader.click();
        }
        Poller.waitUntilTrue(advancedOptions.timed().isVisible());
    }
}