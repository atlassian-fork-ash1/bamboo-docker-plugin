package it.com.atlassian.bamboo.plugins.docker.tasks;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.BambooTestedProductFactory;
import com.atlassian.bamboo.pageobjects.pages.global.BambooDashboardPage;
import com.atlassian.bamboo.testutils.TestBuildDetailsHelper;
import com.atlassian.bamboo.testutils.config.BambooEnvironmentData;
import com.atlassian.bamboo.testutils.junit.rule.BackdoorRule;
import com.atlassian.bamboo.testutils.junit.rule.DetectCapabilitiesRule;
import com.atlassian.bamboo.testutils.model.TestBuildDetails;
import com.atlassian.bamboo.testutils.vcs.git.GitRepositoryDescriptor;
import com.atlassian.bamboo.testutils.vcs.git.LocalGitSetupHelper;
import com.atlassian.bamboo.webdriver.TestInjectionTestRule;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.util.concurrent.LazyReference;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import org.junit.Rule;

public abstract class AbstractDockerTaskTest
{
    private static final BambooEnvironmentData environmentData = new WebDriverTestEnvironmentData();
    protected static final BambooTestedProduct product = BambooTestedProductFactory.create();
    protected static final LazyReference<GitRepositoryDescriptor> repository = new LazyReference<GitRepositoryDescriptor>()
    {
        @Override
        protected GitRepositoryDescriptor create()
        {
            return LocalGitSetupHelper.createRepositoryFromZip("test-repository.zip");
        }
    };

    @Rule public final TestInjectionTestRule injectionRule = new TestInjectionTestRule(this, product);
    @Rule public final BackdoorRule backdoor = new BackdoorRule(environmentData);
    @Rule public final WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();
    @Rule public final DetectCapabilitiesRule detectCapabilitiesRule = new DetectCapabilitiesRule(environmentData);

    protected TestBuildDetails createAndSetupPlan() throws Exception
    {
        final TestBuildDetails plan = TestBuildDetailsHelper.makeUniquePlanTemplate()
                .withScriptTask()
                .withGit(repository.get())
                .withManualBuild()
                .withNoInitialBuild()
                .build();
        backdoor.plans().createPlan(plan);
        product.gotoLoginPage().loginAsSysAdmin(BambooDashboardPage.class);

        return plan;
    }
}