package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.export.BuildImageTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RegistryTaskExporter;
import com.atlassian.bamboo.plugins.docker.export.RunContainerTaskExporter;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.specs.api.builders.AtlassianModule;
import com.atlassian.bamboo.specs.api.builders.task.AnyTask;
import com.atlassian.bamboo.specs.api.model.task.TaskProperties;
import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import com.atlassian.bamboo.specs.builders.task.DockerBuildImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPullImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerPushImageTask;
import com.atlassian.bamboo.specs.builders.task.DockerRunContainerTask;
import com.atlassian.bamboo.task.export.TaskDefinitionExporter;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

@RunWith(JUnitParamsRunner.class)
public class DockerExporterFactoryImplTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private static final BuildImageTaskExporter buildImageTaskExporter = mock(BuildImageTaskExporter.class);
    private static final RegistryTaskExporter registryTaskExporter = mock(RegistryTaskExporter.class);
    private static final RunContainerTaskExporter containerTaskExporter = mock(RunContainerTaskExporter.class);

    @Mock
    private DockerExporterFactory exporterFactory;

    @Before
    public void setUp() throws Exception {
        exporterFactory = new DockerExporterFactoryImpl(
                buildImageTaskExporter,
                registryTaskExporter,
                containerTaskExporter
        );
    }

    @Test
    @Parameters(method = "dockerTaskExporterFromProperties")
    public <T extends Exception> void testCreateDockerTaskExporterFromTaskProperties(final TaskProperties taskProperties, final Class<TaskDefinitionExporter> exporterClass, final Class<T> maybeException) throws Exception {
        if (maybeException != null) {
            expectedException.expect(maybeException);
        }

        TaskDefinitionExporter taskExporter = exporterFactory.createExporter(taskProperties);

        assertThat(taskExporter, isA(exporterClass));
    }

    private Object dockerTaskExporterFromProperties() {
        return new Object[]{
                new Object[]{buildImageProperties(), BuildImageTaskExporter.class, null},
                new Object[]{runProperties(), RunContainerTaskExporter.class, null},
                new Object[]{pullProperties(), RegistryTaskExporter.class, null},
                new Object[]{pushProperties(), RegistryTaskExporter.class, null},
                new Object[]{(anyTaskProperties()), null, IllegalArgumentException.class}
        };
    }

    private TaskProperties buildImageProperties() {
        return EntityPropertiesBuilders.build(new DockerBuildImageTask()
                .imageName("docker-hub.com")
                .dockerfileInWorkingDir());
    }

    private Object runProperties() {
        return EntityPropertiesBuilders.build(new DockerRunContainerTask()
                .imageName("atlassian/fisheye:latest"));
    }

    private Object pushProperties() {
        return EntityPropertiesBuilders.build(new DockerPushImageTask()
                .dockerHubImage("docker-hub.com"));
    }

    private Object pullProperties() {
        return EntityPropertiesBuilders.build(new DockerPullImageTask()
                .dockerHubImage("docker-hub.com"));
    }

    private Object anyTaskProperties() {
        return EntityPropertiesBuilders.build(new AnyTask(new AtlassianModule("plugin:key-module")));
    }

    @Test
    @Parameters(method = "dockerTaskExporterFromCommandOption")
    public <T extends Exception> void testCreateDockerTaskExporterFromCommandOption(final String commandOption, final Class<TaskDefinitionExporter> exporterClass, Class<T> maybeException) throws Exception {
        if (maybeException != null) {
            expectedException.expect(maybeException);
        }

        TaskDefinitionExporter taskExporter = exporterFactory.createExporter(commandOption);

        assertThat(taskExporter, instanceOf(exporterClass));
    }

    private Object dockerTaskExporterFromCommandOption() {
        return new Object[]{
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_BUILD, BuildImageTaskExporter.class, null},
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_RUN, RunContainerTaskExporter.class, null},
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PULL, RegistryTaskExporter.class, null},
                new Object[]{DockerCliTaskConfigurator.DOCKER_COMMAND_OPTION_PUSH, RegistryTaskExporter.class, null},
                new Object[]{"not valid", null, IllegalArgumentException.class},
        };
    }
}