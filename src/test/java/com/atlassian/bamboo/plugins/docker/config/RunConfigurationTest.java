package com.atlassian.bamboo.plugins.docker.config;

import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.docker.DataVolume;
import com.atlassian.bamboo.docker.PortMapping;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RunConfigurationTest
{
    @Test
    public void testFromContext() throws Exception
    {
        final String image = "test/image";
        final String command = "ls";
        final String name = "myimage";
        final String envVars = "TEMP=test";
        final String additionalArgs = "--memory=\"64m\"";
        final String workDir = "/data";

        final Map<String, String> configMap = Maps.newHashMap();
        configMap.put("image", image);
        configMap.put("command", command);
        configMap.put("detach", "true");
        configMap.put("name", name);
        configMap.put("hostPort_0", "8888");
        configMap.put("containerPort_0", "8080");
        configMap.put("hostPort_1", "5000");
        configMap.put("containerPort_1", "5000");
        configMap.put("hostPort_2", "");
        configMap.put("containerPort_2", "443");
        configMap.put("containerPort_3", "80");
        configMap.put("link", "true");
        configMap.put("serviceWait", "true");
        configMap.put("serviceUrlPattern", "http://localhost:${docker.port}");
        configMap.put("serviceTimeout", "50");
        configMap.put("envVars", envVars);
        configMap.put("additionalArgs", additionalArgs);
        configMap.put("workDir", workDir);
        configMap.put("hostDirectory_0", "${bamboo.working.directory}");
        configMap.put("containerDataVolume_0", "/data");
        configMap.put("hostDirectory_1", "");
        configMap.put("containerDataVolume_1", "/data2");
        configMap.put("containerDataVolume_2", "/data3");

        final CommonTaskContext taskContext = mock(CommonTaskContext.class);
        when(taskContext.getConfigurationMap()).thenReturn(new ConfigurationMapImpl(configMap));

        final RunConfiguration runConfig = RunConfiguration.fromContext(taskContext);

        assertThat(runConfig.getImage(), equalTo(image));
        assertThat(runConfig.getCommand(), equalTo(command));
        assertThat(runConfig.isRunDetached(), is(true));
        assertThat(runConfig.getName(), equalTo(name));
        assertThat(runConfig.getFirstPort(), equalTo(new PortMapping(8080, 8888)));
        assertThat(runConfig.getPorts(), containsInAnyOrder(new PortMapping(8080, 8888), new PortMapping(5000, 5000), new PortMapping(443, null), new PortMapping(80, null)));
        assertThat(runConfig.isLink(), is(true));
        assertThat(runConfig.isWaitForService(), is(true));
        assertThat(runConfig.getServiceUrl(), equalTo("http://localhost:${docker.port}"));
        assertThat(runConfig.getServiceTimeout(), equalTo(50L));
        assertThat(runConfig.getEnvironmentVariables(), equalTo(envVars));
        assertThat(runConfig.getAdditionalArgs(), equalTo(additionalArgs));
        assertThat(runConfig.getWorkDir(), equalTo(workDir));
        assertThat(runConfig.getVolumes(), containsInAnyOrder(new DataVolume("${bamboo.working.directory}", "/data"), new DataVolume(null, "/data2"), new DataVolume(null, "/data3")));
    }

    @Test
    public void testFromContextDetachFalse() throws Exception
    {
        final String image = "test/image";
        final String command = "ls";
        final String name = "myimage";
        final String envVars = "TEMP=test";
        final String additionalArgs = "--memory=\"64m\"";

        final Map<String, String> configMap = Maps.newHashMap();
        configMap.put("image", image);
        configMap.put("command", command);
        configMap.put("detach", "false");
        configMap.put("name", name);
        configMap.put("hostPort_0", "8888");
        configMap.put("containerPort_0", "8080");
        configMap.put("hostPort_1", "5000");
        configMap.put("containerPort_1", "5000");
        configMap.put("link", "true");
        configMap.put("serviceWait", "true");
        configMap.put("serviceUrlPattern", "http://localhost:${docker.port}");
        configMap.put("serviceTimeout", "50");
        configMap.put("envVars", envVars);
        configMap.put("additionalArgs", additionalArgs);

        final CommonTaskContext taskContext = mock(CommonTaskContext.class);
        when(taskContext.getConfigurationMap()).thenReturn(new ConfigurationMapImpl(configMap));

        final RunConfiguration runConfig = RunConfiguration.fromContext(taskContext);

        assertThat(runConfig.getImage(), equalTo(image));
        assertThat(runConfig.getCommand(), equalTo(command));
        assertThat(runConfig.isRunDetached(), is(false));
        assertThat(runConfig.getName(), is(nullValue())); // no name because detach is false
        assertThat(runConfig.getFirstPort(), is(nullValue())); // no ports because detach is false
        final List<PortMapping> expectedPorts = ImmutableList.of(); // no ports because detach is false
        assertThat(runConfig.getPorts(), equalTo(expectedPorts));
        assertThat(runConfig.isLink(), is(true));
        assertThat(runConfig.isWaitForService(), is(false)); // no wait because detach is false
        assertThat(runConfig.getServiceUrl(), equalTo("http://localhost:${docker.port}"));
        assertThat(runConfig.getServiceTimeout(), equalTo(50L));
        assertThat(runConfig.getEnvironmentVariables(), equalTo(envVars));
        assertThat(runConfig.getAdditionalArgs(), equalTo(additionalArgs));
    }
}