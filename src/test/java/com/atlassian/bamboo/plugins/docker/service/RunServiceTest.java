package com.atlassian.bamboo.plugins.docker.service;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.plugins.docker.PollingService;
import com.atlassian.bamboo.plugins.docker.client.Docker;
import com.atlassian.bamboo.plugins.docker.tasks.cli.DockerCliTaskConfigurator;
import com.atlassian.bamboo.process.EnvironmentVariableAccessor;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.CurrentResult;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import com.atlassian.bamboo.variable.VariableDefinitionContextImpl;
import com.atlassian.bamboo.variable.VariableType;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RunServiceTest
{
    @Rule
    public final MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Docker docker;

    @Mock
    private EnvironmentVariableAccessor environmentVariableAccessor;

    @Mock
    private PollingService pollingService;

    @Mock
    private CommonTaskContext taskContext;

    private Map<String, VariableDefinitionContext> effectiveVariables;

    @InjectMocks
    private RunService runService;

    @Before
    public void setup()
    {
        when(taskContext.getBuildLogger()).thenReturn(mock(BuildLogger.class));

        final CommonContext commonContext = mock(CommonContext.class);
        final VariableContext variableContext = mock(VariableContext.class);
        final CurrentResult currentResult = mock(CurrentResult.class);
        this.effectiveVariables = new HashMap<>();

        when(taskContext.getCommonContext()).thenReturn(commonContext);

        when(commonContext.getCurrentResult()).thenReturn(currentResult);
        when(commonContext.getVariableContext()).thenReturn(variableContext);

        when(variableContext.getEffectiveVariables()).thenReturn(effectiveVariables);
    }

    @Test
    public void testExecuteCurrentDetachedContainerNotIncludedInLinks() throws Exception
    {
        effectiveVariables.put("dockerContainerNames", new VariableDefinitionContextImpl("dockerContainerNames", "one two", VariableType.RESULT));

        final ConfigurationMap configurationMap = new ConfigurationMapImpl();
        configurationMap.put(DockerCliTaskConfigurator.NAME, "three");
        configurationMap.put(DockerCliTaskConfigurator.DETACH, Boolean.TRUE.toString());
        configurationMap.put(DockerCliTaskConfigurator.LINK, Boolean.TRUE.toString());
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);

        runService.execute(taskContext);

        verify(docker).isRunning("one");
        verify(docker).isRunning("two");
        verify(docker, times(0)).isRunning("three");
    }
}
